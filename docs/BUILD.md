## Сборка
При сборке устанавливай разрешение 1024х768 в девтулзах, так ты можешь заметить косяки в верстке.
Основные моменты, которые надо смотреть при сборке - сценарий, статистика, неиспользуемые компоненты (их быть не должно).


### Сценарий
* В сценарии описывается все поведение презентации - переходы между слайдами, первый слайд для Layout, нужные препараты в допменю, допслайды. Вообще все, кроме рефов.

1. **Сценарий презентации** прописывается в `src/components/Navigation`. Там есть `scenario.js`, это входной и выходной файл для всех сценариев, которые должны лежать в той же папке. Механизм работы следующий: 
  - В файле презентации прописывается `const name='название презы'`.
  - Это название экспортируется (внизу конфига, вместе с остальным полями).
  - Все, что экспортируется из сценария, импортируется в `scenario.js` как `import * as название_переменной from './файл_презентации.js'`.
  - Переменная добавляется в массив `allScenarios`.
  - Затем отфильтровывается нужный сценарий. Этот нужный сценарий задается в переменной `currPres`. В нее записывается либо глобальная переменная `process.env.REACT_APP_CURRENT_PRESENTATION`, либо строка (которую мы прописываем вручную, и которая должна быть идентична `name` в нужном нам сценарии). Глобальная переменная работает только при `npm run build`, поэтому для сервера разработки используется эта самая строка. Затем через filter мы отбираем объект сценария, в котором `name` равен этой строке, и экспортируем его.

2. **Собственно сам объект сценария** - со слайдами и ветками - это SCEN_OBJECT. Он состоит из двух частей - того, что мы написали сами (тут все обычно), и деструктуризации результата вызова функции `getBranchesFromDopData`. Туда мы передаем отфильтрованные данные из `src/dop-slides-data.js`. 
- В `dop-slides-data.js` лежит вся информация для допменю - пдфки и слайды. Она разбита по препаратам, если надо что-то добавить - делай все по образцу, как там. Эти данные потом используются в компоненте `Layout`, для рендеринга меню. Для допслайдов должны быть прописаны через запятую ветки - `'hel_03_dop,hel_1'`, функция `getBranchesFromDopData` возьмет их и через деструктуризацию запихнет в SCEN_OBJECT. 
- Вся эта инфа фильтруется в сценарии - нам, к примеру, в одной презе нужны одни препараты, а в другой - другие. В массиве `activeProductsFirstSlideLinks` указываются объекты, у которых `name` - название нужного нам продукта, а `firstSlide` - первый слайд этого продукта в презе. `firstSlide` может быть либо строкой, либо объектом. В первом случае он прямо запишется в пункт меню, во втором - в различных ключах мы можем указывать первый слайд продукта для разных веток (к примеру, 4 визита, и в каждом визите у препарата свой первый слайд). Это обрабатывается в функции `getHrefForFirstSlide` в Layout, там настраивается, как именно нам брать имя визита.
- После того, как в `activeProductsFirstSlideLinks` указаны нужные нам продукты, они фильтруются в `filteredDopMenuData` (через jsonparse-jsonstringify, чтобы был новый объект, а не ссылка на оригинальный). Там же мы можем дополнительно что-то добавить, убрать для каждого продукта, если надо.

3. **initSlide** = это самый первый слайд в презе, не забываем поставить нужный. 

4. **Ребра**. Из файла сценария мы управляем переходами по кнопкам и свайпам.
- Ребро может быть либо объектом, либо функцией, которая возвращает объект. Объект имеет такой вид: `{slide: 'какой-то слайд', scene: 'какая-то ветка'}`. В функции мы можем задавать дополнительные проверки и т.п., и возвращать что надо в зависимости от условий.
- Переходами по кнопкам мы управляем через атрибут `data-store`. Он задается на кнопке (значение любое, тип - строка), по которой должен происходить переход. При клике по такой кнопке это значение записывается в `window.nav._slideStore`, с ключом в виде имени слайда. Затем в функции ребра для конкретного слайда мы берем `window.nav.getStoreItem('конкретный слайд')`, и в зависимости от значения возвращаем то или иное ребро. Если нужно, чтобы на слайде не сохранялось это значение, то в `componentDidMount` пишем такой код: `window.nav._slideStore = {}`, вуаля.

### Статистика.

1. Статистика пишется внутри слайдов, см [ридми про верстку](./docs/PSDTOREACT.md). Что важно знать при сборке:
- Есть файл `product-dictionary.json`, он используется для автоматического построения статистики по просмотренным продуктам. Метод, который это делает, находится в `src/components/Statistic`, в функции `getViewedProductsQuestion`. Он берет все слайды из сценария, сплитит их по нижнему подчеркиванию, и берет элиас слайда (первые буквы, которые обозначают продукт), затем на основе их смотрит, какие продукты есть в презе и какие были просмотрены. Если надо отфильтровать слайды с необычным названием и что-то такое - это делается там. **ВАЖНО**, чтобы в этом вопросе по показанным продуктам не было нигде `undefined`. Все вопросы можно посмотреть в `window.store.questions`.
- При сборке нужно прощелкать все слайды, убедиться, что нигде не прилетает никаких ошибок, связанных с вопросами, нигде нет `undefined`, и т.п.

### Layout и неиспользуемые компоненты.
На момент написания ридмихи у нас есть два типа глобала, какой используется - зависит от проекта. В `index.js` приложения надо оставить тот, который нужен, а импорт второго удалить - чтобы он не попадал в билд в принципе. 
**Почему это важно.** Платформа Oncall, в которую мы грузим презы, может решить в какой-то момент (поскольку это произошло один раз, и кто знает, что там будет в будущем), что из-за неиспользуемых картинок и прочих элементов оно не будет работать. Поэтому в приложении **не должно быть неиспользуемых импортов, которые  используют в себе картинки (слайды, рендерящиеся компоненты)**. Поэтому же у нас есть скрипт `rewriteIndex`, который оставляет в `index.js `слайдов только те, что нам действительно нужны.
- Нужно проверить, чтобы к слайдам подтянулись нужные иконки препаратов(в левом верхнем углу) и чтобы текст меню был нужного цвета. Все эти параметры настраиваются в файле Layout.js. Если настроек в файле не хватает, то придется колхозить свои. Иконки препаратов зависят от названия слайда.
- Убеждаемся, что для продуктов правильно прилетела `filteredDopMenuData`, что все допслайды правильно сделались и что везде правильно проставились ссылки на первые слайды продукта. Превьюшек в допслайдов не будет, пока не сделаешь `npm run screens`.

### Источники (они же рефы)
**Заполняем рефы,** если они не заполнены. Они лежат в массиве `src/reference-data.js` и заполняем массивами, где имя массива это имя слайда, а элементы массива это сами референсы. Если нужен перенос на новую строку с интервалом, то создаем еще один элемент массива, если интервал не нужен, то вставляем <br/>.
- В `index.js` есть функция `compareRefs`, которая проверяет наличие источников у слайдов в текущем сценарии, и консольложит их. Пройдись по массиву, который она отдаст, проверь, почему там нет рефов (может, по дизайну и нет их в принципе).
