export const getStoreItem = (slide = window.router._currSlide) => window.router.getStoreItem(slide);

export const getProductsData = (scenario, dopSlidesData, updatedProductsData) => {
  const branchNamesAndValues = Object.entries(scenario);
  const branchNames = Object.keys(scenario);
  const branches = Object.values(scenario);

  const getFirstProductSlide = (productName, index) => {
    if (index === 0) return `${scenario.default[0]},default`; // TODO пояснить что для первого продукта первый слайд всегда начальный
    const { productSlideAlias } = dopSlidesData.find((product) => product.productName === productName);
    const firstProductBranch = branchNames.find((branchName) =>
      productSlideAlias.some((alias) => branchName.includes(alias)),
    );
    if (firstProductBranch) return `${scenario[firstProductBranch][0]},${firstProductBranch}`;
    else {
      const [firstBranch, slidesInBrnach] = branchNamesAndValues.find(([branchName, branch]) => {
        return (
          productSlideAlias.some((alias) => branchName.includes(alias)) ||
          branch.some((slide) => productSlideAlias.some((alias) => slide.includes(alias)))
        );
      });
      const indexFirstSlideInBranch = slidesInBrnach.findIndex((slide) =>
        productSlideAlias.some((alias) => slide.includes(alias)),
      );
      return `${scenario[firstBranch][indexFirstSlideInBranch]},${firstBranch}`;
    }
  };

  const updaterProductsData = ({ name }) => {
    const productIndex = dopSlidesData.findIndex(({ productName }) => productName === name);
    if (productIndex >= 0) {
      const { firstSlide, slides } = updatedProductsData;
      if (firstSlide) dopSlidesData[productIndex].firstSlide = firstSlide;
      if (slides) dopSlidesData[productIndex].slides = slides;
    } else console.error(`в dop-slides-data.js нет продукта - '${name}', Может, опечатка?`);
  };

  return branches
    .reduce((acc, branch) => {
      branch.forEach((slide) => {
        const alias = slide.split('_')[0];
        const product = dopSlidesData.find(({ productSlideAlias }) => productSlideAlias.includes(alias));
        product && acc.push(product.productName);
      });
      return [...new Set(acc)];
    }, [])
    .map((productName, index) => {
      if (Boolean(updatedProductsData.length)) {
        updatedProductsData.forEach(updaterProductsData);
      }
      return {
        firstSlide: getFirstProductSlide(productName, index),
        ...dopSlidesData.find((product) => product.productName === productName),
      };
    })
    .filter(({ productName }) => productName);
};

export function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split('&');
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    // eslint-disable-next-line
    if (decodeURIComponent(pair[0]) == variable) {
      return decodeURIComponent(pair[1]);
    }
  }
  console.log('Query variable is not found', variable);
}

function getUniqSlides(scen) {
  return [...new Set(Object.keys(scen).reduce((acc, scenKey) => acc.concat(scen[scenKey]), []))];
}

function getUnwantedSlide(slides, scene, getUniqSlides) {
  const uniqSlideFromScen = getUniqSlides(scene);
  return slides.filter((slide) => uniqSlideFromScen.indexOf(slide) === -1);
}

export function getMissingSlide(slides, scene, getUniqSlides) {
  const uniqSlideFromScen = getUniqSlides(scene);
  return uniqSlideFromScen.filter((slide) => slides.indexOf(slide) === -1);
}

export function getBranchesFromDopData(dopMenuData) {
  return dopMenuData.reduce((accumForScenario, currData) => {
    return {
      ...accumForScenario,
      ...currData.slides.reduce(reduceScenarioFromDopDataSlides, {}),
    };
  }, {});

  function reduceScenarioFromDopDataSlides(acc, curr) {
    if (curr.type === 'slides') {
      curr.groupSlides.map((link) => {
        const [slide, scene] = link.split(',');
        if (acc.hasOwnProperty(scene)) {
          acc[scene].push(slide);
        } else {
          acc[scene] = [slide];
        }
        return acc;
      });
    }
    return acc;
  }
}

export function checkSlides(slides, scenario) {
  slides = Object.keys(slides);
  const allSlides = getUniqSlides(scenario);
  allSlides.forEach((slideName) => {
    if (!slides.includes(slideName)) {
      throw new Error(`Не экспортирован слайд ${slideName}. Запусти "npm run update".`);
    }
  });
  const unwantedSlides = getUnwantedSlide(slides, scenario, getUniqSlides);
  if (unwantedSlides.length !== 0) {
    throw new Error(`В экспорте/импорте есть лишние слайды. Сделай "npm run update"`);
  }
}

export function getHrefForFirstSlide(product, currScene) {
  const { firstSlide } = product;
  if (typeof firstSlide === 'string') return firstSlide;
  else if (typeof firstSlide === 'function') return firstSlide();
  else return firstSlide[currScene.substring(0, 7)];
}
