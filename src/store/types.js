export const types = {
  HEADER_SETTINGS: 'headerSettings',
  PRODUCTS_DATA: 'productsData',
  CURR_VALUES: 'currValues',
  DOP_INFO: 'dopInfo',
  PRODUCT: 'product',
  GLOBAL: 'global',
  SLIDE: 'slide',
  ALIAS: 'alias',
  SCENE: 'scene',
  REFS: 'refs',
};
