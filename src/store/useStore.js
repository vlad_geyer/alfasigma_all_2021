import { useCallback, useReducer } from 'react';

import { reducer } from './reducer';
import { types } from './types';

const initialState = {
  [types.HEADER_SETTINGS]: {
    hideLyaout: false,
  },
  [types.CURR_VALUES]: {
    [types.SLIDE]: '',
    [types.SCENE]: '',
    [types.ALIAS]: '',
  },
  [types.DOP_INFO]: [],
  [types.PRODUCTS_DATA]: [],
  [types.REFS]: [],
};

export const useStore = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const setStore = useCallback((type = types.SET_STATE, payload) => {
    dispatch({ type, payload });
  });

  return [state, setStore];
};
