export const reducer = (state, { type, payload }) => {
  return (
    {
      ...state,
      [type]: payload,
    } || state
  );
};
