import React from 'react';
import { render } from 'react-dom';
import { Router } from 'react-router-dom';
import { createHashHistory } from 'history';
import initReactFastclick from 'react-fastclick';
import { RouterStada } from '@onpoint-dev/mpa-router';

import * as config from './pres_config/scenario';
import * as slides from './slides';
import SwipeTrack from './components/SwipeTrack/SwipeTrack';
import { getQueryVariable } from './utils/utils';
import { checkSlides } from './utils/utils';

import App from './App';

document.addEventListener(
  'touchmove',
  (e) => {
    e.preventDefault();
  },
  { passive: false },
);

const { scenario, ribs } = config;

initReactFastclick();
checkSlides(slides, scenario);

export const history = createHashHistory();

window.router = new RouterStada({
  getQueryVariable,
  isStada: true,
  scenario,
  history,
  ribs,
});

const OPTIONS = {
  right: () => window.router.prev(),
  left: () => window.router.next(),
};
window.swipeTrack = new SwipeTrack(OPTIONS);

render(
  <Router history={history}>
    <App config={config} />
  </Router>,
  document.getElementById('root'),
);
