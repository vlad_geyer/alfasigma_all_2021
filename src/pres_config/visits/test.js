import dopSlidesData from '@onpoint-dev/stada-builder/payloads/dop-slides-data';

import { getBranchesFromDopData, getProductsData, getStoreItem } from '../../utils/utils';

const config = {
  presName: 'test',
  // globalType: 'default', // burger/mensHealth
};

const updatedProductsData = [];

const initScens = {
  default: ['al_02_2105', 'al_03_2105'],
};

const ribs = {
  slide_test: {
    next: () => {
      const vars = {
        1: 'default',
      };
      return vars[getStoreItem()] || 'next';
    },
  },
};

const [initSlide] = initScens.default;
const productsData = getProductsData(initScens, dopSlidesData, updatedProductsData);
const scenario = { ...initScens, ...getBranchesFromDopData(productsData) };

export { scenario, initSlide, ribs, productsData, config };
