import 'colors';
import * as scenarios from './index.js';

const currPres = process.env.REACT_APP_CURRENT_PRESENTATION || 'test';
const scenarioToExport = scenarios[currPres];
const { ribs, scenario, initSlide, productsData, config } = scenarioToExport;

if (!scenarioToExport) {
  console.log(
    `
Ты попытался собрать сценарий ${currPres}, но что-то пошло не так.

Проверь:
1) Что сценарий импортируется и экспортируется в pres_config/index.js;
2) Что название сценария совпадает со строкой currPres в pres_config/scenario.js;
2.1) Проверь название и строку на кириллические символы.
`.yellow,
  );
}

export { ribs, scenario, initSlide, productsData, config, currPres };
