import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import logos from '../../assets/logos';
import icons from '../../assets/icons';

import { Footer } from './Footer';
import { Header } from './Header';

export default function Global({ state, globalClickHandler, initSlide }) {
  const { refs } = state;
  const images = {
    logos: logos,
    icons: icons,
  };
  return (
    <GlobalLayout>
      <Footer refs={refs} globalClickHandler={globalClickHandler} images={images} />
      <Header logos={logos} initSlide={initSlide} />
    </GlobalLayout>
  );
}

const GlobalLayout = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;
