import React, { useState } from 'react';
import styled from 'styled-components';

export function Header({ logos, initSlide }) {
  const { alfa_normix } = logos;
  return (
    <>
      <Logo>
        <img alt='logo' src={alfa_normix} data-href={initSlide}></img>
      </Logo>
    </>
  );
}

const Logo = styled.div`
  img {
    position: absolute;
    width: 272px;
    height: 42px;
    top: 105px;
    right: 80px;
  }
`;
