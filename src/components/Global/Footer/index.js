import React, { useState } from 'react';
import styled from 'styled-components';

import recomendations from '../../../assets/pdfs/recomendations.pdf';

import RefsPopup from './RefsPopup';

/*global tbClmEvent, openDocument, location*/
export const Footer = ({ refs, globalClickHandler, images }) => {
  const { logos, icons } = images;
  const { alfasigma } = logos;
  const { burger, ref, spc } = icons;

  const [refsShow, setRefsShow] = useState(false);

  const hideAll = () => {
    setRefsShow(false);
  };

  const toggleMenuItem = (e) => {
    const { item } = e.target.dataset;
    globalClickHandler(e);
    hideAll();
    item === 'refs' && setRefsShow(!refsShow);
    tbClmEvent('showModal', 'modal01');
  };

  return (
    <>
      <FooterLayout>
        <AlfaSigmaLogo>
          <img alt='alfaSigmaLogo' onClick={() => window.open('https://www.google.com')} src={alfasigma} />
        </AlfaSigmaLogo>
        <Icons>
          <img alt='icon' src={burger} onClick={() => tbClmEvent('showDoc', 'bundled document name')}></img>
          <img alt='icon' src={ref} data-item='refs' data-pdf={recomendations}></img>
          <img alt='icon' src={spc} onClick={() => openDocument('recomendations', 'test')}></img>
        </Icons>
      </FooterLayout>
      {refsShow && <RefsPopup refs={refs} />}
    </>
  );
};

const FooterLayout = styled.div`
  position: absolute;
  width: 100%;
  height: 90px;
  bottom: 0;
`;

const AlfaSigmaLogo = styled.div`
  img {
    position: absolute;
    width: 125px;
    height: 27px;
    bottom: 30px;
    left: 30px;
  }
`;

const Icons = styled.div`
  position: absolute;
  display: flex;
  justify-content: space-between;
  bottom: 30px;
  right: 30px;
  width: 150px;

  img {
    width: 30px;
    height: 30px;
  }
`;
