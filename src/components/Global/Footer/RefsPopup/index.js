import React from 'react';
import styled from 'styled-components';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll';

const RefsPopup = ({ refs }) => {
  return (
    <LayoutRefsPopup>
      <ReactIScroll
        iScroll={iScroll}
        options={{
          mouseWheel: true,
          scrollbars: true,
        }}
      >
        <div style={{ paddingBottom: '30px' }}>
          {refs.map((ref) => (
            <RefsText key={ref} dangerouslySetInnerHTML={{ __html: `${ref}` }} />
          ))}
        </div>
      </ReactIScroll>
    </LayoutRefsPopup>
  );
};

const LayoutRefsPopup = styled.div`
  position: absolute;
  width: 84%;
  background-color: white;
  height: 69%;
`;

const RefsText = styled.p`
  font-family: 'Open Sans';
  font-size: 17px;
  margin-top: 10px;
  line-height: 1.25em;
  user-select: auto !important;

  :nth-child(1) {
    margin-top: 0;
  }

  a {
    text-decoration: none;
    color: black;
    user-select: auto !important;
  }
`;

export default RefsPopup;
