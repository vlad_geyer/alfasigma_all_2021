import React, { useState, useEffect } from 'react';
import './UrlChecker.scss';

export default function UrlChecker({ hashHistory, initUrl }) {
  const [url, setUrl] = useState('');
  useEffect(() => {
    hashHistory.listen(() => {
      setUrl(window.location.href);
    });
  }, []);
  return (
    <div className='url'>
      Url is :<span>{url === '' ? initUrl : url}</span>
    </div>
  );
}
