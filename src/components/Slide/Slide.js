import React, { Component } from 'react';

import { types } from '../../store';

export default class Slide extends Component {
  /**
   * @param {object} options
   * @param {array} options.ref для установки источников на слайде ['1. example text', '2. example text']
   * @param {array} options.questions для работы со статистикой на слайде, смотри README.md
   * @param {Object{type: String, slide_id: String, title: String, crm_sync: Boolean, answers:{answerName:String}}[]} options.questions
   * @param {object} options.header для кастомизации хедера
   * @param {boolean} options.header.hideLayout
   * @param {boolean} options.header.menuIsWhite
   * @param {boolean} options.header.refBtnIsInverted
   * @param {boolean} options.header.menuBtnIsInverted
   * @param {boolean} options.header.noBrandLogo
   * @param {boolean} options.header.brandLogoIsWhite
   * @param {boolean} options.header.stadaLogoIsWhite
   * @param {boolean} options.header.stadaLogoIsBlue
   * @param {boolean} options.header.headerIsTransparent
   *
   */
  constructor({ ref, questions, header, action, slideName, products }) {
    super();
    this.questions = questions;
    this.products = products;
    this.state = { slideState: 0, slideName: slideName };
    action(types.HEADER_SETTINGS, header || {});
    action(types.REFS, ref || []);
    if (this.questions && this.questions.length) {
      this.validateQuestions();
    }
  }

  //Фолбэк для старых слайдов, чтобы не сломались
  changeState = (e) => {
    const { state } = e.target.dataset;
    state &&
      this.setState({
        slideState: state,
        prevState: this.state.slideState,
      });
  };

  //Universal Click Handler
  slideClickHandler = (e) => {
    const { state, metr, stat, store } = e.target.dataset;
    console.log('StoreValue:', store);
    if (state) {
      const newState = state;
      this.setState({
        slideState: newState,
        prevState: this.state.slideState,
      });
    }
    if (metr || stat) {
      const dataStat = metr || stat;
      const qa = dataStat.split(',');
      const question = this.questions[qa[0]];
      const answer = qa[1];
      const selectOption = qa[2];
      this.props.statistics.setStatistic(question, answer, selectOption);
    }
    if (store) {
      window.router.setStoreItem(window.router._currSlide, store);
      return window.router.next();
    }
    this.props.globalClickHandler(e);
  };
}
