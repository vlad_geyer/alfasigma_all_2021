import React, { useEffect, useState } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { types, useStore } from './store';

import Global from './components/Global';
import * as pdfs from './assets/pdfs';
import * as slides from './slides';

import './styles/index.scss';
import './assets/fonts/fonts.scss';
import '@onpoint-dev/stada-builder/payloads/animate.scss';
import '@onpoint-dev/stada-builder/payloads/custom-animations.scss';

export default function App({ config }) {
  const [store, setStore] = useStore();
  const { initSlide } = config;

  const globalClickHandler = (e) => {
    let elementWithPdf = e.target.closest('[data-pdf]');
    let linkElem = e.target.closest('[data-href]');
    // let { pdf, href } = e.target.dataset;

    if (elementWithPdf) {
      let pdf = elementWithPdf.dataset.pdf;
      // Обработчик пдф, напрямую импортированных в слайд
      if (pdf.includes('static')) {
        if (typeof Android === 'undefined') {
          window.open(pdf);
        } else {
          window.Android.showPdf(pdf);
        }
      } else {
        // Обработчик обычной строки в data-pdf
        pdf = pdf.replace(/\//gm, '_');
        console.log(pdf);
        console.log(pdfs);
        if (typeof Android === 'undefined') {
          window.open(pdfs[pdf]);
        } else {
          window.Android.showPdf(pdfs[pdf]);
        }
      }
    }

    if (linkElem) {
      let linkStr = linkElem.dataset.href.split(',');
      let slide = linkStr[0];
      let flow = linkStr[1];

      let allSlides = Object.keys(slides);
      if (!allSlides.includes(slide)) throw new Error(`Попытался перейти на ${slide}, а его и нет.`);
      const slideId = window.router._currSlide;
      if (slideId !== slide) {
        window.router.to(slide, flow);
      }
    }
  };

  useEffect(() => {
    setStore(types.CURR_VALUES, {
      [types.SLIDE]: window.router._currSlide,
      [types.SCENE]: window.router._sceneName,
      [types.ALIAS]: window.router._currSlide.split('_')[0],
    });
    console.log('Presentation State: ', store);
  }, [window.router._currSlide]);

  return (
    <div className='app' onClick={globalClickHandler}>
      <div className='slide-container'>
        {Object.keys(slides).map((slideName) => {
          const Slide = slides[slideName];
          return (
            <Route
              path={'/' + slideName}
              key={slideName}
              render={() => (
                <>
                  <Slide action={setStore} globalClickHandler={globalClickHandler} slideName={slideName} />
                </>
              )}
            />
          );
        })}
        <Redirect to={'/' + initSlide + '?scene=default'} />
        {<Global state={store} globalClickHandler={globalClickHandler} initSlide={initSlide} />}
      </div>
    </div>
  );
}
