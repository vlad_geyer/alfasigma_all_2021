import React from 'react';
import Slide from '../../components/Slide/Slide.js';
import './al_07_2105.scss';

export default class al_07_2105 extends Slide {
  constructor(props) {
    super({ header, ...props, dopInfo, ref });
  }

  /*
  componentDidMount() {
    window.nav.setStoreItem(window.nav._currSlide, null);
  }
  */

  render() {
    const { slideState } = this.state;
    return (
      <div
        className={'slide al_07_2105 al_07_2105_state_' + slideState}
        ref='al_07_2105'
        data-state={slideState}
        onClick={this.slideClickHandler}
      >
        <div className='scale-wrapper'>
          <div className='rectangle3' />
          <div className='brn_close' />
          <div className='tittle' />
        </div>
      </div>
    );
  }
}

const header = {
  headerIsTransparent: true,
};

const dopInfo = [];

/* 
const questions = [
  {
    type: 'VARIABILITY',
    slide_id: 'al_07_2105',
    title: 'Была ли нажата кнопка ""?',
    crm_sync: true,
    answers: {
      opened: 'Да',
    },
  },
]; 
*/

const ref = undefined;
