import React from 'react';
import Slide from '../../components/Slide/Slide.js';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import './al_02_2105.scss';

export default class al_02_2105 extends Slide {
  constructor(props) {
    super({ header, ...props, ref });
    this.state = {
      slideState: 0,
      value: 0,
    };
  }

  /*
  componentDidMount() {
    window.nav.setStoreItem(window.nav._currSlide, null);
  }
  */
  onSliderChange = (value) => {
    this.setState({
      slideState: value,
      value: value,
    });
  };

  render() {
    const { slideState } = this.state;
    return (
      <div
        className={'slide al_02_2105 al_02_2105_state_' + slideState}
        ref='al_02_2105'
        data-state={slideState}
        onClick={this.slideClickHandler}
      >
        <div className='scale-wrapper'>
          <div className='woman_1 f' />
          <div className='woman_2 f' />
          <div className='rings f' />
          <div className='txt_centr f' />
          <div className='icn_31 f d2' />
          <div className='icn_5 f d10' />
          <div className='icn_11 f d4' />
          <div className='icn_21 f d8' />
          <div className='icn_4 f d6' />
          <div className='icn_1 f d2' />
          <div className='icn_2 f d4' />
          <div className='icn_3 f d6' />
          <div className='line f' />
          <div className='left f' />
          <div className='block_1 f' />
          <div className='block_2 f' />
          <div className='btn f' data-state='2' />
          <div className='tittle fl' />
          <div className='popup_content f' />
          <div className='popup_btn_close f' data-state='0' />

          <Slider
            className='slider'
            min={0}
            max={1}
            value={this.state.value}
            onChange={this.onSliderChange}
            railStyle={{
              height: 0,
            }}
            handleStyle={{
              height: 47,
              width: 47,
              background: '#9c1729',
              border: 0,
            }}
            trackStyle={{
              background: 'transparent',
            }}
          />
          <div className='finger' />
        </div>
      </div>
    );
  }
}

const header = {
  headerIsTransparent: true,
};

const ref = [
  '* на 14 день лечения в основной группе',
  '1. Карпова Е. П., Соколова М. В. Ирригационная терапия аллергического ринита у детей//Российская оториноларингология. – 2007. - №5. – с. 163-167',
];
