require = require('esm')(module);
const fs = require('fs');
const hostUrl = 'https://trueblue.onpoint.ru/archives/test_trueblue';
const zipNames = fs.readdirSync('./zip');
console.log(process.env);
const data = {
  stories: [
    {
      storyName: 'Onpoint test story',
      thumbnailUrl: `${hostUrl}/thumb.jpg`,
      documentFolders: [
        {
          folderName: 'test',
          documents: [
            {
              documentName: 'recomendations',
              documentUrl: `${hostUrl}/recomendations.pdf`,
            },
          ],
        },
      ],
      visuals: zipNames.map((zipName) => {
        return {
          visualName: zipName,
          visualUrl: `${hostUrl}/${zipName}`,
        };
      }),
    },
  ],
};

const dataJSON = JSON.stringify(data, null, '\t');

fs.writeFileSync('./zip/TBTest.json', dataJSON);
