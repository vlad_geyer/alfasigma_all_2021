#!/bin/bash

JSTMP="import React from 'react';
import Slide from '../../components/slide/slide.js';
import './$1.scss';


export default class $1 extends Slide {
  constructor(props) {
    super({...props})
  }
  
  render() {
    const { slideState } = this.state;
    return (
      <div className={'slide $1 $1_' + slideState} ref='$1' onClick={this.slideClickHandler}>
      <div className='scale_wrapper'></div>
      </div>
    );
  }
}"
CSSTMP=".$1 {
  /*background: url('./img/bg.jpg') 0 0/contain no-repeat;*/
}

@media (width: 1024px) {
  .$1 {

  }
}"

mkdir src/slides/$1 &&
mkdir src/slides/$1/img -p &&
touch src/slides/$1/$1.js &&
touch src/slides/$1/$1.scss &&
sed -i -e "s/}/  $1,\n}/g" ./src/slides/index.js
sed -i -e "s/export {/import $1 from '\.\/$1\/$1'\n\nexport \{/" ./src/slides/index.js
echo "$JSTMP" >>  ./src/slides/$1/$1.js &&
echo "$CSSTMP" >>  ./src/slides/$1/$1.scss